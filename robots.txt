User-agent: GoogleBot
Disallow: /vendor/*
Disallow: /admin/*

User-agent: Yandex
Disallow: /vendor/*
Disallow: /admin/*

User-agent: bingbot
Disallow: /vendor/*
Disallow: /admin/*

Sitemap: https://www.phpstore.com.ua/sitemap.xml
Host: https://www.phpstore.com.ua