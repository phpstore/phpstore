$('.btn-create-folder').magnificPopup({
    type:'inline',
    midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});

$('.btn-popup-create-folder').on('click', function () {
    let inputName = $('#popup-name-dir input');
    let dirName = inputName.val();
    if ($.trim(dirName).length === 0) {
        $('p.error').css('display', 'block');
    } else {
        $('p.error').css('display', 'none');

        loadingOn();
        $.ajax({
            url: window.location.pathname,
            data: {
                action: 'createDir',
                name: dirName
            },
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                loadingOff();
                if (response.status === 'success') {
                    let dirUrl = response.data.url;
                    $('.dir-items .row').append('<div class="col-lg-1">\n' +
                        '                        <div class="dir-item">\n' +
                        '                            <a href="#">\n' +
                        '                                <div class="dir-item">\n' +
                        '                                    <i class="fas fa-folder"></i>\n' +
                        '                                    <p><a href="' + dirUrl + '">' + dirName + '</a></p>\n' +
                        '                                </div>\n' +
                        '                            </a>\n' +
                        '                        </div>\n' +
                        '                    </div>');
                    $.magnificPopup.close();
                    inputName.val('');
                } else {
                    alert('Ошибка при создании директории. Попробуйте позже');
                    inputName.val('');
                }
            },
            error: function () {
                loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
                inputName.val('');
            }
        });
    }
    return false;
});

$('.btn-delete-folder').on('click', function () {
    if (!confirm('Вы уверены что хотите удалить директорию со всеми картинками?')) {
        return false;
    }

    loadingOn();
    $.ajax({
        url: window.location.pathname,
        data: {
            action: 'deleteDir'
        },
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            if (response.status === 'success') {
                alert('Директория успешно удалена!');
                window.location.href = response.data.url;
            }
            loadingOff();
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });

    return false;
});