$('.product-text').richText({
    fontSize: true
});

$('.btn-insert-notice').on('click', function () {
    $('.richText-editor').append('&lt;div class="article-notice" &gt;&lt;i class="fas fa-exclamation-circle"&gt;&lt;/i&gt;&lt;/div&gt;');
    return false;
});

$('.btn-insert-img').on('click', function () {
    $('.richText-editor').append('&lt;div&gt;&lt;a href=&quot;{{img-_}}&quot; data-fancybox=&quot;gallery&quot; data-caption=&quot;_&quot;&gt;&lt;img src=&quot;{{img-_}}&quot; alt=&quot;{{alt-_}}&quot;/&gt;&lt;/a&gt;&lt;/div&gt;');
    return false;
});

$('.btn-insert-console').on('click', function () {
    $('.richText-editor').append('&lt;pre class="article-console" &gt;&lt;/pre&gt;');
    return false;
});

$('.btn-insert-code').on('click', function () {
    $('.richText-editor').append('&lt;pre class="article-code" &gt;&lt;/pre&gt;');
    return false;
});

$('.btn-add-image').on('click', function () {
    $('.table-images tbody').append('<tr class="row-article-image">\n' +
        '                        <td><input type="text" name="path"/></td>\n' +
        '                        <td><input type="text" name="alt"/></td>\n' +
        '                        <td><input type="text" name="position"/></td>\n' +
        '                        <td style="text-align: center; width: 100px;">\n' +
        '                            <a href="" class="btn-delete-image"><span class="i-delete"><i class="fas fa-trash-alt"></i></span></a>\n' +
        '                        </td>\n' +
        '                    </tr>');

    return false;
});

$('.btn-save-article').on('click', function () {
    let article = getArticleData();
    if (article === false) {
        return false;
    }

    loadingOn();
    $.ajax({
        url: $(location).attr("href"),
        dataType: 'json',
        type: 'POST',
        data: {
            product: article,
            action: 'save'
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert(ERROR_MESSAGE_AJAX_BROKEN);
        }
    });
    return false;
});

$('.table-images').on('click', '.btn-delete-image', function () {
    if (!confirm('Вы уверены что хотите удалить картинку?')) {
        return false;
    }
    let imageId = $(this).data('image-id');
    if (isNaN(imageId)) {
        $(this).closest('tr').remove();
    } else {
        loadingOn();
        $.ajax({
            url: $(location).attr("href"),
            dataType: 'json',
            type: 'POST',
            data: {
                action: 'deleteArticleImage',
                imageId: imageId,
            },
            success: function (response) {
                loadingOff();
                alert(response.message);
            },
            error: function () {
                loadingOff();
                alert(ERROR_MESSAGE_AJAX_BROKEN);
            }
        });
    }
    return false;
});

function getArticleData()
{
    let isValid = true;
    let generalData = $('.form-general').serializeArray();
    if (validateForm(generalData) === false) {
        alert(ERROR_MESSAGE_EMPTY_FIELDS);
        isValid = false;
    }
    generalData = formatKeyValueArray(generalData);

    let tableImages = $('.table-images');
    let rowsImages = tableImages.find('tr.row-article-image');
    let articleImages = [];
    if (rowsImages.length !== 0) {
        rowsImages.each(function () {
            let path = $(this).find('input[name=path]').val();
            let alt = $(this).find('input[name=alt]').val();
            let position = $(this).find('input[name=position]').val();
            if (!$.isNumeric(position)) {
                alert('Position должен быть числом!');
                isValid = false;
            }
            articleImages.push({path: path, alt: alt, position: position});
        });
    }
    let article = generalData;
    article['images'] = articleImages;

    return isValid ? article : isValid;
}