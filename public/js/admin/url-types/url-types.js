$('.btn-create-url-type').on('click', function () {
    let data = $('.form-general').serializeArray();

    if (validateForm(data) === false) {
        alert('Все поля должны быть заполнены!');
        return false;
    }
    let urlTypeData = formatKeyValueArray(data);

    loadingOn();
    $.ajax({
        url: '/admin/url-type/create/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            urlType: urlTypeData
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });

    return false;
});

$('.btn-edit-url-type').on('click', function () {
    let data = $('.form-general').serializeArray();

    if (validateForm(data) === false) {
        alert('Все поля должны быть заполнены!');
        return false;
    }
    let urlTypeData = formatKeyValueArray(data);

    let urlTypeId = $(this).data('url-type-id');
    urlTypeData['id'] = urlTypeId;

    loadingOn();
    $.ajax({
        url: '/admin/url-type/edit/' + urlTypeId + '/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            urlType: urlTypeData
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });

    return false;
});

$('.btn-url-type-delete').on('click', function () {
    if (!confirm('Вы уверены что хотите удалить url type?')) {
        return false;
    }

    loadingOn();
    let urlTypeId = $(this).data('url-type-id');
    $.ajax({
        url: '/admin/url-type/delete/' + urlTypeId + '/',
        dataType: 'json',
        type: 'POST',
        success: function (response) {
            loadingOff();
            alert(response.message);
            location.reload();
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже!');
        }
    });

    return false;
});