$('#url-type-all').on('change', function(){
    let checkboxes = $(this).closest('.block-url-types').find(':checkbox');
    checkboxes.prop('checked', $(this).is(':checked'));
});

$('.btn-run-generator').on('click', function(){
    let selectedUrlTypes = [];
    $('.list-url-types input:checked').each(function(){
        selectedUrlTypes.push($(this).data('url-type'));
    });
    if (selectedUrlTypes.length === 0) {
        alert('Выбирите url тип для генерации ссылок!');
        return false;
    }

    loadingOn();
    $.ajax({
        url: '/admin/url-generator/?action=run',
        dataType: 'json',
        type: 'POST',
        data: {
            selectedUrlTypes: selectedUrlTypes
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже!');
        }
    })

    // $.ajax({
    //     url: '/admin/url-type/edit/' + urlTypeId + '/?action=save',
    //     dataType: 'json',
    //     type: 'POST',
    //     data: {
    //         urlType: urlTypeData
    //     },
    //     success: function (response) {
    //         loadingOff();
    //         alert(response.message);
    //     },
    //     error: function () {
    //         loadingOff();
    //         alert('Что то пошло не так! Попробуйте позже');
    //     }
    // });

    return false;
});