$('.btn-create-department').on('click', function () {
    let generalData = $('.form-general').serializeArray();
    let metaData = $('.form-meta-data').serializeArray();
    let departmentData = $.merge(generalData, metaData);

    if (validateForm(departmentData) === false) {
        alert('Все поля должны быть заполнены!');
        return false;
    }
    departmentData = formatKeyValueArray(departmentData);

    loadingOn();
    $.ajax({
        url: '/admin/department/create/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            department: departmentData
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });

    return false;
});

$('.btn-delete-department').on('click', function () {
    if (!confirm('Вы уверены что хотите удалить департмент?')) {
        return false;
    }

    loadingOn();
    let departmentId = $(this).data('department-id');
    $.ajax({
        url: '/admin/department/delete/' + departmentId + '/',
        dataType: 'json',
        type: 'POST',
        data: {
            id: departmentId
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
            location.reload();
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже!');
        }
    });

    return false;
});

$('.btn-edit-department').on('click', function () {
    let generalData = $('.form-general').serializeArray();
    let metaData = $('.form-meta-data').serializeArray();
    let departmentData = $.merge(generalData, metaData);

    if (validateForm(departmentData) === false) {
        alert('Все поля должны быть заполнены!');
        return false;
    }

    departmentData = formatKeyValueArray(departmentData);

    let departmentId = $('.input-department-id').data('department-id');
    departmentData['id'] = departmentId;

    loadingOn();
    $.ajax({
        url: '/admin/department/edit/' + departmentId + '/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            department: departmentData
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });

    return false;
});