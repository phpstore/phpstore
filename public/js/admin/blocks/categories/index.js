$('.selector-departments').on('change', function () {
    let departmentId = $('.selector-departments option:selected').val();
    $('.btn-show-category-elements').attr('href', '/admin/block/categories/?action=show&departmentId=' + departmentId);
});

$('.btn-save-elements').on('click', function () {
    let rowsElement = $('.table-elements').find('tr.row-element');
    let urlParams = new URLSearchParams(window.location.search);
    let departmentId = urlParams.get('departmentId');
    let elements = {};
    rowsElement.each(function () {
        let elementId = parseInt($(this).data('element-id'));
        let categoryId = parseInt($(this).data('category-id'));
        let elementData = $(this).find('form').serializeArray();
        elementData = formatKeyValueArray(elementData);
        elementData['id'] = elementId;
        elementData['categoryId'] = categoryId;
        elementData['departmentId'] = departmentId;
        elements[elementId] = elementData;
    });

    loadingOn();
    $.ajax({
        url: '/admin/block/categories/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            elements: elements
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });
    return false;
});