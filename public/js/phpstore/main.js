$('.btn-mmenu').on('click', function () {
    $('#main-menu').mmenu({
        "navbar": {
            "title": "<p class='logo'>PHP<span>STORE</span></p><p class='description'>САЙТ О PHP</p>"
        },
        "extensions": [
           "theme-dark",
           "pagedim-black",
           "border-full"
        ]
    });
});

function loadingOn()
{
    loading = new Loading({

        // // 'ver' or 'hor'
        // direction: 'ver',
        //
        // // loading title
        // title: undefined,
        //
        // // text color
        // titleColor: '#FFF',
        //
        // // font size
        // titleFontSize: 14,
        //
        // // extra class(es)
        // titleClassName: undefined,
        //
        // // font family
        // titleFontFamily:   undefined,
        //
        // // loading description
        // discription: undefined,
        //
        // // text color
        // discriptionColor:  '#FFF',
        //
        // // font size
        // discriptionFontSize: 14,
        //
        // // extra class(es)
        // discriptionClassName: undefined,
        //
        // // font family
        // directionFontFamily: undefined,
        //
        // // width/height of loading indicator
        // loadingWidth: 'auto',
        // loadingHeight: 'auto',

        // padding in pixels
        loadingPadding: 8,

        // // background color
        // loadingBgColor: '#252525',
        //
        // // border radius in pixels
        loadingBorderRadius: 4,
        //
        // // loading position
        // loadingPosition: 'fixed',
        //
        // // shows/hides background overlay
        // mask: true,
        //
        // // background color
        // maskBgColor: 'rgba(0, 0, 0, .6)',
        //
        // // extra class(es)
        // maskClassName: undefined,
        //
        // // mask position
        // maskPosition: 'fixed',
        //
        // // 'image': use a custom image
        // // loading<a href="https://www.jqueryscript.net/animation/">Animation</a>: 'origin',
        //
        // // path to loading spinner
        // animationSrc: undefined,
        //
        //     // width/height of loading spinner
        animationWidth: 35,
        animationHeight: 35,
        // animationOriginWidth: 4,
        // animationOriginHeight: 4,
        //
        // // color
        // animationOriginColor: '#FFF',
        //
        // // extra class(es)
        // animationClassName: undefined,
        //
        // // auto display
        // defaultApply: true,
        //
        // // animation options
        // animationIn: 'animated fadeIn',
        // animationOut: 'animated fadeOut',
        // animationDuration:  1000,
        //
        // // z-index property
        // zIndex: 0
    });
}

function loadingOff()
{
    loading.out();
}

function validateForm(array)
{
    let bool = true;
    if (array.length === 0) {
        bool = false;
    } else {
        $.each(array, function (key, value) {
            if ($.trim(value).length === 0 || $.trim(value.value).length === 0) {
                bool = false;
            }
        });
    }

    return bool;
}

// $('.btn-search').on('click', function () {
//     let query = $('.input-search').val();
//
//     $.ajax({
//         url: '/search/?q=' + query,
//         type: 'GET',
//         dataType: 'json',
//         success: function (response) {
//             console.log('response');
//             // loadingOff();
//             // alert(response.message);
//             // location.reload();
//         },
//         error: function () {
//             alert('Что то пошло не так! Попробуйте позже');
//         }
//     });
//
//     return false;
// });

$('.btn-search-popup-link').magnificPopup({
    type:'inline',
    alignTop: true,
    focus: '#focus-search-input'
});

$('.btn-products-sorting').magnificPopup({
    type:'inline',
    midClick: true
});

$('.btn-search-popup').on('click', function () {
    let query = $('.input-search-popup').val();
    if ($.trim(query).length === 0) {
        return false;
    }
    window.location.href = window.location.origin + '/search/?q=' + query;

    return false;
});

$('.input-search-popup').on('keypress', function (e) {
    if (e.keyCode === 13) {
        let query = $('.input-search-popup').val();
        if ($.trim(query).length === 0) {
            return false;
        }
        let url = window.location.origin + '/search/?q=' + query;
        $(location).attr('href', url);

        return false;
    }
});

$('#products-sorting-selector-popup ul li').on('click', function () {
    if ($(this).hasClass('active')) {
        return false;
    }
    let sort = $(this).find('span').data('sort');
    let url = document.location.protocol +"//" + window.location.hostname + window.location.pathname + '?sort=' + sort;

    loadingOn();

    window.location.href = url;

    return false;
});

$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('.scrollup').fadeIn();
    } else {
        $('.scrollup').fadeOut();
    }
});

$('.scrollup').on('click',function () {
    $('html, body').animate({ scrollTop: 0 }, 600);
    return false;
});