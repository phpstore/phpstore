$('.category-item').on('mouseover',function () {
    $(this).find('p.title').css('text-decoration', 'underline');
});
$('.category-item').on('mouseout',function () {
    $(this).find('p.title').css('text-decoration', 'none');
});

$('.selector-sorting-articles').on('change', function () {
    loadingOn();
    let sort = $(this).val();
    window.location.href = window.location.pathname + '?sort=' + sort;

    return false;
});