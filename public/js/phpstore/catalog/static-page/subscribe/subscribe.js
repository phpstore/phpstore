$('.departments-selector').on('change', function () {
    let departmentId = $(this).val();
    let selectorCategories = $('.categories-selector');
    selectorCategories.find('option').remove().end().append('<option value="0">Все</option>');
    if (departmentId == 0) {
        selectorCategories.attr('disabled', true);
        return false;
    }

    loadingOn();
    $.ajax({
        url: '/subscribe/',
        dataType: 'json',
        type: 'POST',
        data: {
            action: 'loadCategories',
            departmentId: departmentId
        },
        success: function (response) {
            let categories = response.categories;
            $.each(categories, function (categoryId, category) {
                selectorCategories.append('<option value="' + categoryId + '">' + category.name + '</option>')
            });
            $('.categories-selector').prop('disabled', false);
            loadingOff();
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });

    return false;
});