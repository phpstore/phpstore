$('.selector-sorting-articles').on('change', function () {
    loadingOn();
    let sort = $(this).val();
    window.location.href = window.location.pathname + '?sort=' + sort;

    return false;
});