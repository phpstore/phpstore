let blockRating = $('.rating');

function getCountUp(count)
{
    let ratingCount = $('.rating-count');
    if (count === 0) {
        ratingCount.removeClass('count-red').addClass('count-green');
        count = '+' + (count + 1);
    } else if (count === -1) {
        ratingCount.removeClass('count-red');
        count = 0;
    } else if (count > 0) {
        count = '+' + (count + 1);
        ratingCount.removeClass('count-green').addClass('count-green');
    } else if (count < 0) {
        count = count + 1;
        ratingCount.removeClass('count-red').addClass('count-red');
    }

    return count;
}

function getCountDown(count)
{
    let ratingCount = $('.rating-count');
    if (count === 0) {
        ratingCount.removeClass('count-green').addClass('count-red');
        count = -1;
    } else if (count === 1) {
        ratingCount.removeClass('count-green');
        count = 0;
    } else if (count > 0) {
        count = '+' + (count - 1);
        ratingCount.removeClass('count-green').addClass('count-green');
    } else if (count < 0) {
        count = count - 1;
        ratingCount.removeClass('count-red').addClass('count-red');
    }

    return count;
}

blockRating.on('click', '.fa-arrow-alt-circle-up', function () {
    let ratingCount = $('.rating-count');
    let count = parseInt(ratingCount.text());
    let action = 'set';
    let vote = 0;
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        count = getCountDown(count);
        action = 'delete';
    } else {
        if ($('.fa-arrow-alt-circle-down').hasClass('active')) {
            $('.fa-arrow-alt-circle-down').removeClass('active');
            count = count + 1;
        }
        $(this).addClass('active');
        count = getCountUp(count);
        vote = 1;
    }
    ratingCount.text(count);

    let articleId = $('.article').data('article-id');
    $.ajax({
        url: '/prod/article/update/rating/',
        data: {
            action: action,
            articleId: articleId,
            rating: count,
            vote: vote
        },
        type: 'POST',
        dataType: 'json',
        success: function (response) {
        },
        error: function () {
        }
    });

    return false;
});

blockRating.on('click', '.fa-arrow-alt-circle-down', function () {
    let ratingCount = $('.rating-count');
    let count = parseInt(ratingCount.text());
    let action = 'set';
    let vote = 0;
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        count = getCountUp(count);
        action = 'delete';
    } else {
        if ($('.fa-arrow-alt-circle-up').hasClass('active')) {
            $('.fa-arrow-alt-circle-up').removeClass('active');
            count = count - 1;
        }
        $(this).addClass('active');
        count = getCountDown(count);
        vote = 0;
    }
    ratingCount.text(count);

    let articleId = $('.article').data('article-id');
    $.ajax({
        url: '/prod/article/update/rating/',
        data: {
            action: action,
            articleId: articleId,
            rating: count,
            vote: vote
        },
        type: 'POST',
        dataType: 'json',
        success: function (response) {
        },
        error: function () {
        }
    });

    return false;
});