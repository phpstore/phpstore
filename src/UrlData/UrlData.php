<?php

namespace App\UrlData;

use App\Repository\UrlsRepository;

class UrlData
{
    /** @var int */
    private $urlId;

    /** @var string */
    private $url;

    /** @var bool */
    private $open;

    /** @var int */
    private $urlTypeId;

    /** @var int */
    private $departmentId;

    /** @var int */
    private $categoryId;

    /** @var int */
    private $productId;

    /** @var int */
    private $entityId;

    public function __construct(array $urlDataArray)
    {
        $urlDataArray += [
            UrlsRepository::FIELD_ID => 0,
            UrlsRepository::FIELD_URL => '',
            UrlsRepository::FIELD_OPEN => 0,
            UrlsRepository::PROPERTY_URL_TYPE_ID => 0,
            UrlsRepository::PROPERTY_DEPARTMENT_ID => 0,
            'categoryId' => 0,
            'productId' => 0,
            UrlsRepository::PROPERTY_ENTITY_ID => 0
        ];
        $this->urlId = $urlDataArray[UrlsRepository::FIELD_ID];
        $this->url = $urlDataArray[UrlsRepository::FIELD_URL];
        $this->open = $urlDataArray[UrlsRepository::FIELD_OPEN];
        $this->urlTypeId = $urlDataArray[UrlsRepository::PROPERTY_URL_TYPE_ID];
        $this->departmentId = $urlDataArray[UrlsRepository::PROPERTY_DEPARTMENT_ID];
        $this->categoryId = $urlDataArray['categoryId'];
        $this->productId = $urlDataArray['productId'];
        $this->entityId = $urlDataArray[UrlsRepository::PROPERTY_ENTITY_ID];
    }

    /**
     * @return int
     */
    public function getUrlId(): int
    {
        return $this->urlId;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return bool
     */
    public function isOpen(): bool
    {
        return $this->open;
    }

    /**
     * @return int
     */
    public function getUrlTypeId(): int
    {
        return $this->urlTypeId;
    }

    /**
     * @return int
     */
    public function getDepartmentId(): int
    {
        return $this->departmentId;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }
}
