<?php

namespace App\Controller\PhpStore;

use App\Repository\ArticlesRatingRepository;
use App\Repository\UserArticleRatingRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxController extends AbstractController
{
    /**
     * @param Request $request
     * @param UserArticleRatingRepository $userArticleRatingRepository
     * @param ArticlesRatingRepository $articlesRatingRepository
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateArticleRating(
        Request $request,
        UserArticleRatingRepository $userArticleRatingRepository,
        ArticlesRatingRepository $articlesRatingRepository
    ): JsonResponse {
        $action = $request->get('action');
        $articleId = (int) $request->get('articleId');
        $rating = (int) $request->get('rating');
        $vote = (int) $request->get('vote');

        $ip = $request->getClientIp();

        switch ($action) {
            case 'set':
                $userArticleRatingRepository->setUserVote($ip, $articleId, $vote);
                break;
            case 'delete':
                $userArticleRatingRepository->delete($ip, $articleId);
                break;
        }

        $articlesRatingRepository->setArticleRating($articleId, $rating);

        return new JsonResponse([]);
    }
}
