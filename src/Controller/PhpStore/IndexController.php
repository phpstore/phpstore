<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 28.12.2018
 * Time: 17:02
 */

namespace App\Controller\PhpStore;

use App\Entity\Departments;
use App\Repository\ArticlesRepository;
use App\Repository\CategoriesRepository;
use App\Repository\StaticPagesRepository;
use App\Service\Category\CategoriesService;
use App\Service\ProductLoader\ProductLoaderFactory;
use App\Service\ProductLoader\ProductLoaderService;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends BaseController
{
    /**
     * @param StaticPagesRepository $staticPagesRepository
     * @param ArticlesRepository $articlesRepository
     * @param ProductLoaderFactory $productLoaderFactory
     * @return Response
     */
    public function index(
        StaticPagesRepository $staticPagesRepository,
        ArticlesRepository $articlesRepository,
        ProductLoaderFactory $productLoaderFactory
    ): Response {
        $page = $staticPagesRepository->find($this->urlData->getEntityId());
        if ($page === null) {
            throw $this->createNotFoundException('Страница не найдена');
        }
        $meta = [
            'title' => $page->getTitle(),
            'keywords' => $page->getKeywords(),
            'description' => $page->getDescription(),
            'h1' => $page->getH1()
        ];

        $topArticleIds = $articlesRepository->getTopViewsArticleIds(5);
        $articleProductLoader = $productLoaderFactory->buildByDepartmentId(Departments::DEPARTMENT_ID_ARTICLE);
        $articles = $articleProductLoader->getProductsByIds($topArticleIds);
        $topArticles = [];
        foreach ($topArticleIds as $topArticleId) {
            foreach ($articles as $index => $article) {
                if ($topArticleId == $article[ArticlesRepository::FIELD_ID]) {
                    $text = $article['text'];
                    $text = ProductLoaderService::getDecodeText($text);
                    $text = ProductLoaderService::getShortText($text, 400);
                    $article['text'] = $text;
                    $topArticles[] = $article;
                }
            }
        }

        return $this->render('phpstore/base.html.twig', [
            'topArticles' => $topArticles,
            'meta' => $meta
        ]);
    }
}