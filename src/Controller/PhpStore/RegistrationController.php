<?php

namespace App\Controller\PhpStore;

class RegistrationController extends BaseController
{
    public function registration()
    {
        $submit = $this->request->get('submit');
        if ($submit) {
            $name = $this->request->get('name');
            $email = $this->request->get('email');
            $password = $this->request->get('password');
        }
        return $this->render('phpstore/catalog/static-page/registration/registration.html.twig');
    }
}
