<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 23.03.2019
 * Time: 17:07
 */

namespace App\Controller\PhpStore;

use App\Repository\ArticlesViewsRepository;
use App\Repository\UserArticleRatingRepository;
use App\Repository\UserArticleVisitsRepository;
use App\Service\ProductLoader\Loaders\ArticleProductsLoader;
use App\Service\ProductLoader\ProductLoaderService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends BaseController
{
    /**
     * @param ArticleProductsLoader $articleProductsLoader
     * @param ArticlesViewsRepository $articlesViewsRepository
     * @param UserArticleVisitsRepository $userArticleVisitsRepository
     * @param UserArticleRatingRepository $userArticleRatingRepository
     * @param ProductLoaderService $productLoaderService
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function product(
        ArticleProductsLoader $articleProductsLoader,
        ArticlesViewsRepository $articlesViewsRepository,
        UserArticleVisitsRepository $userArticleVisitsRepository,
        UserArticleRatingRepository $userArticleRatingRepository,
        ProductLoaderService $productLoaderService
    ): Response {
        $productId = $this->urlData->getProductId();
        $userIp = $this->request->getClientIp();
        $visitId = $userArticleVisitsRepository->getIdByIpAndArticleId($userIp, $productId);
        if (empty($visitId)) {
            $userArticleVisitsRepository->setUserArticleVisit($userIp, $productId);
            $articlesViewsRepository->updateViewByProductId($productId);
        }
        $vote = $userArticleRatingRepository->getVoteByArticleIdAndIp($productId, $userIp);

        $product = $articleProductsLoader->getProductById($productId);
        $product['text'] = ProductLoaderService::getDecodeText($product['text']);
        $product = $productLoaderService->applyProductImages($product);
        $product['vote'] = $vote;

        return $this->render($this->getBaseTemplatePath() . 'product/article.html.twig', [
            'product' => $product
        ]);
    }
}
