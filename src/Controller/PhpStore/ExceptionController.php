<?php

namespace App\Controller\PhpStore;

use App\Service\Menu\MenuService;
use App\UrlData\UrlData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ExceptionController extends AbstractController
{
    public function render404Page(
        MenuService $menuService
    ): Response {
        $urlData = new UrlData([]);
        $menu = $menuService->loadMenu($urlData);
        return $this->render('bundles/TwigBundle/Exception/error404.html.twig', [
            'menu' => $menu
        ]);
    }
}
