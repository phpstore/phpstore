<?php

namespace App\Controller\Admin;

use App\Entity\Articles;
use App\Repository\ArticlesImagesRepository;
use App\Repository\ArticlesRepository;
use App\Repository\CategoriesRepository;
use App\Repository\UrlsRedirectRepository;
use App\Service\DepartmentCategory\DepartmentCategoriesService;
use App\Service\Department\DepartmentService;
use App\Service\ProductLoader\ProductLoaderFactory;
use App\Service\UrlGenerator\UrlLoaders\Loaders\ProductUrlLoader;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ProductsController extends BaseController
{
    public const LOAD_CATEGORIES = 'loadCategories';
    public const ALLOWED_PRODUCT_STATUS = ['in progress', 'published'];

    /**
     * @param DepartmentService $departmentService
     * @param DepartmentCategoriesService $departmentCategoriesService
     * @param ProductLoaderFactory $productLoaderFactory
     * @return JsonResponse|Response
     */
    public function products(
        DepartmentService $departmentService,
        DepartmentCategoriesService $departmentCategoriesService,
        ProductLoaderFactory $productLoaderFactory
    ) {
        $action = $this->request->get('action');
        if (!empty($action)) {
            $response = [];
            switch ($action) {
                case self::LOAD_CATEGORIES:
                    $departmentId = $this->request->get('departmentId');
                    $departmentRelations = $departmentCategoriesService->getDepartmentRelationsByDepartmentId(
                        $departmentId
                    );
                    $categories = $departmentRelations[DepartmentCategoriesService::KEY_CATEGORIES];
                    $response = ['categories' => $categories];
                    break;
            }

            return new JsonResponse($response);
        }

        $departments = $departmentService->getAllDepartments();
        $firstDepartmentId = key($departments);
        $departmentRelations = $departmentCategoriesService->getDepartmentCategoryRelations();
        $selectedDepartmentId = (int) $this->request->get('departmentId');
        foreach ($departments as $departmentId => $department) {
            if (!isset($departmentRelations[$departmentId])) {
                unset($departments[$departmentId]);
            }
            $departments[$departmentId]['selected'] = false;
            if ($selectedDepartmentId === $departmentId) {
                $departments[$departmentId]['selected'] = true;
            }
        }
        $firstDepartmentRelations = $departmentCategoriesService->getDepartmentRelationsByDepartmentId(
            $firstDepartmentId
        );
        $categories = $firstDepartmentRelations[DepartmentCategoriesService::KEY_CATEGORIES];
        $selectedCategoryId = (int) $this->request->get('categoryId');
        foreach ($categories as &$category) {
            $category['selected'] = false;
            if ($selectedCategoryId === $category[CategoriesRepository::FIELD_ID]) {
                $category['selected'] = true;
            }
        }

        $products = [];
        if (!empty($firstDepartmentId)) {
            $productLoader = $productLoaderFactory->buildByDepartmentId($firstDepartmentId);
            if (!empty($selectedCategoryId)) {
                $products = $productLoader->getProductsByCategoryId($selectedCategoryId);
            } else {
                $products = $productLoader->getProducts();
            }
        }

        return $this->render('admin/products/products.html.twig', [
            'departments' => $departments,
            'categories' => $categories,
            'products' => $products,
            'selectedDepartment' => $firstDepartmentRelations
        ]);
    }

    public function create(
        ProductLoaderFactory $factory,
        DepartmentCategoriesService $departmentCategoriesService
    ) {
        $departmentId = (int) $this->request->get('departmentId');
        $productLoader = $factory->buildByDepartmentId($departmentId);
        $template = $productLoader->getAdminProductTemplate() . 'create.html.twig';

        $action = $this->request->get('action');
        if (!empty($action)) {
            $message = 'Неверный action! Попробуйте позже!';
            switch ($action) {
                case self::ACTION_SAVE:
                    $product = $this->request->get('product');
                    $productLoader->setProduct($product);
                    $message = 'Продукт успешно создан!';
                    break;
            }

            return new JsonResponse([self::KEY_MESSAGE => $message]);
        }

        $relations = $departmentCategoriesService->getDepartmentRelationsByDepartmentId($departmentId);
        $categories = $relations[DepartmentCategoriesService::KEY_CATEGORIES];
        $selectedCategoryId = (int) $this->request->get('categoryId');
        foreach ($categories as &$category) {
            $category['selected'] = false;
            if ($selectedCategoryId === $category[CategoriesRepository::FIELD_ID]) {
                $category['selected'] = true;
            }
        }

        return $this->render($template, [
            'categories' => $categories,
            'status' => self::ALLOWED_PRODUCT_STATUS
        ]);
    }

    /**
     * @param ProductLoaderFactory $factory
     * @param DepartmentCategoriesService $departmentCategoriesService
     * @param ArticlesImagesRepository $articlesImagesRepository
     * @param ArticlesRepository $articlesRepository
     * @param ProductUrlLoader $productUrlLoader
     * @param UrlsRedirectRepository $urlsRedirectRepository
     * @return JsonResponse|Response
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function edit(
        ProductLoaderFactory $factory,
        DepartmentCategoriesService $departmentCategoriesService,
        ArticlesImagesRepository $articlesImagesRepository,
        ArticlesRepository $articlesRepository,
        ProductUrlLoader $productUrlLoader,
        UrlsRedirectRepository $urlsRedirectRepository
    ) {
        $departmentId = (int) $this->request->get('departmentId');
        $productId = (int) $this->request->get('productId');
        if ($productId === null) {
            throw new InvalidArgumentException('Неверный product id!');
        }

        $productLoader = $factory->buildByDepartmentId($departmentId);
        $template = $productLoader->getAdminProductTemplate() . 'edit.html.twig';
        $product = $productLoader->getProductById($productId);
        $product['text'] = htmlspecialchars_decode($product['text']);

        $action = $this->request->get('action');
        if (!empty($action)) {
            $message = 'Неверный action! Попробуйте позже!';
            switch ($action) {
                case self::ACTION_SAVE:
                    $article = $this->request->get('product');
                    $article['id'] = $productId;
                    $articleFromDB = $articlesRepository->getArticleById($productId);
                    $oldH1 = $articleFromDB[Articles::FIELD_H1];
                    $newH1 = $article[Articles::FIELD_H1];
                    if ($oldH1 !== $newH1) {
                        $newUrl = $productUrlLoader->formatUrlFromTitle($newH1);
                        $oldUrl = $productUrlLoader->formatUrlFromTitle($oldH1);
                        $urlsRedirectRepository->setUrlRedirect($oldUrl, $newUrl);
                    }
                    $productLoader->setProduct($article);
                    $message = 'Изменения успешно сохранены!';
                    break;
                case 'deleteArticleImage':
                    $imageId = (int) $this->request->get('imageId');
                    $articlesImagesRepository->deleteImageById($imageId);
                    $message = 'Картинка успешно удалена!';
                    break;
            }

            return new JsonResponse([self::KEY_MESSAGE => $message]);
        }

        $relations = $departmentCategoriesService->getDepartmentRelationsByDepartmentId($departmentId);
        $categories = $relations[DepartmentCategoriesService::KEY_CATEGORIES];
        $selectedCategoryId = (int) $this->request->get('categoryId');
        foreach ($categories as &$category) {
            $category['selected'] = false;
            if ($selectedCategoryId === $category[CategoriesRepository::FIELD_ID]) {
                $category['selected'] = true;
            }
        }

        return $this->render($template, [
            'categories' => $categories,
            'status' => self::ALLOWED_PRODUCT_STATUS,
            'product' => $product
        ]);
    }

    public function delete(
        ProductLoaderFactory $productLoaderFactory
    ) {
        $departmentId = (int) $this->request->get('departmentId');
        $productId = (int) $this->request->get('productId');
        $productLoader = $productLoaderFactory->buildByDepartmentId($departmentId);
        $productLoader->deleteByProductId($productId);
        $message = 'Продукт успешно удален!';

        return $this->json([self::KEY_MESSAGE => $message]);
    }
}
