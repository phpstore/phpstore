<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 01.03.2019
 * Time: 21:19
 */

namespace App\Controller\Admin;

use App\Repository\DepartmentsRepository;
use App\Service\Department\DepartmentService;
use App\Service\Url\UrlService;
use Symfony\Component\HttpFoundation\JsonResponse;

class DepartmentsController extends BaseController
{
    const ACTION_SAVE = 'save';
    const KEY_MESSAGE = 'message';

    public function departments(
        DepartmentService $departmentService
    ) {
        $departments = $departmentService->getAllDepartments();

        return $this->render('admin/departments/departments.html.twig', [
            'departments' => $departments
        ]);
    }

    /**
     * @param $id
     * @param DepartmentService $departmentService
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function edit(
        $id,
        DepartmentService $departmentService
    ) {
        $departmentEntry = $departmentService->getDepartmentEntryById($id);
        if (empty($departmentEntry)) {
            throw $this->createNotFoundException('Department not found');
        }
        $action = $this->request->get('action');
        if (!empty($action)) {
            $message = 'Неверный action. Попробуйте еще раз!';
            switch ($action) {
                case self::ACTION_SAVE:
                    $department = $this->request->get('department');
                    $department[DepartmentsRepository::FIELD_URL]
                        = UrlService::formatTableUri($department[DepartmentsRepository::FIELD_URL]);
                    $departmentService->updateDepartment($department);
                    $message = 'Изменения успешно сохранены!';
                    break;
                default:
                    break;
            }
            return new JsonResponse([self::KEY_MESSAGE => $message]);
        }

        return $this->render('admin/departments/edit.html.twig', [
            'department' => $departmentEntry
        ]);
    }

    /**
     * @param $id
     * @param DepartmentService $departmentService
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(
        $id,
        DepartmentService $departmentService
    ) {
        $department = $departmentService->getDepartmentEntryById($id);
        if (empty($department)) {
            throw $this->createNotFoundException('Department not found');
        }
        $departmentService->deleteDepartmentById($id);
        $message = 'Департмент успешно удален!';

        return new JsonResponse([self::KEY_MESSAGE => $message]);
    }

    /**
     * @param DepartmentService $departmentService
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(
        DepartmentService $departmentService
    ) {
        $action = $this->request->get('action');
        if (!is_null($action)) {
            $message = 'Департмент успешно создан!';
            switch ($action) {
                case self::ACTION_SAVE:
                    $department = $this->request->get('department');
                    $name = $department[DepartmentsRepository::PROPERTY_NAME];
                    if (!empty($departmentService->getDepartmentIdByName($name))) {
                        $message = 'Департмент с таким именем уже существует!';
                        break;
                    }
                    $department[DepartmentsRepository::FIELD_URL]
                        = UrlService::formatTableUri($department[DepartmentsRepository::FIELD_URL]);
                    if (!empty($departmentService->getDepartmentIdByUrl(
                        $department[DepartmentsRepository::FIELD_URL]
                    ))) {
                        $message = 'Департмент с таким url уже существует!';
                        break;
                    }

                    $departmentService->setDepartment($department);
                    break;
                default:
                    break;
            }
            return new JsonResponse(['message' => $message]);
        } else {
            return $this->render('admin/departments/create.html.twig');
        }
    }
}
