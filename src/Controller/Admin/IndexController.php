<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 06.01.2019
 * Time: 21:07
 */

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;

class IndexController extends BaseController
{
    /**
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('admin/index/index.html.twig');
    }
}
