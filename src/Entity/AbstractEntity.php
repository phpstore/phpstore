<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 08.03.2019
 * Time: 14:00
 */

namespace App\Entity;

class AbstractEntity
{
    public function toArray()
    {
        $arrayObject = new \ArrayObject($this);

        $return = array();

        $tmpResult = $arrayObject->getArrayCopy();
        foreach ($tmpResult as $key => $value) {
            if (!is_object($value)) {
                $propertyName = preg_replace('/\0.*\0/', '', $key);

                $return[$propertyName] = $value;
            }
        }

        return $return;
    }
}