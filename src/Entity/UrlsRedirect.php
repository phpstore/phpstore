<?php

namespace App\Entity;

use App\MetaData\UrlsRedirectMetaData;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UrlsRedirectRepository")
 */
class UrlsRedirect implements UrlsRedirectMetaData
{
    public function __construct()
    {
        $this->setDefaultDateTime();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="`from`", type="string", length=255)
     */
    private $from;

    /**
     * @ORM\Column(name="`to`", type="string", length=255)
     */
    private $to;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_update;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from): void
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     */
    public function setTo($to): void
    {
        $this->to = $to;
    }

    public function getLastUpdate(): ?\DateTimeInterface
    {
        return $this->last_update;
    }

    public function setLastUpdate(\DateTimeInterface $last_update): self
    {
        $this->last_update = $last_update;

        return $this;
    }

    public function setDefaultDateTime(): void
    {
        $this->last_update = new \DateTime();
    }
}
