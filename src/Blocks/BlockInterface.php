<?php

namespace App\Blocks;

use App\UrlData\UrlData;

interface BlockInterface
{
    /**
     * @param UrlData $urlData
     * @return array
     */
    public function loadContent(UrlData $urlData): array;
}
