<?php

namespace App\MetaData;

interface BlockElementCategoriesMetaData
{
    public const FIELD_ID = 'id';
    public const FIELD_DEPARTMENT_ID = 'departmentId';
    public const FIELD_CATEGORY_ID = 'categoryId';
    public const FIELD_TITLE = 'title';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_IMG = 'img';
    public const FIELD_ALT = 'alt';
    public const FIELD_ORDER = 'order';
}
