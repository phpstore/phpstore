<?php

namespace App\MetaData;

interface UsersSubscribeMetaData
{
    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const FIELD_EMAIL = 'email';
    public const FIELD_DEPARTMENT_ID = 'departmentId';
    public const FIELD_CATEGORY_ID = 'categoryId';
}
