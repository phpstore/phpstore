<?php

namespace App\MetaData;

interface DepartmentsMetaData
{
    public const FIELD_ID = 'id';
    public const FIELD_URL = 'url';

    public const DEPARTMENT_ID_ARTICLE = 1;
    public const DEPARTMENT_ID_BOOK = 2;
    public const DEPARTMENT_ID_VIDEO = 3;

    public const DEPARTMENT_ARTICLE = 'article';
    public const DEPARTMENT_BOOK = 'book';
}
