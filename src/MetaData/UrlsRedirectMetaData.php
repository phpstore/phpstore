<?php

namespace App\MetaData;

interface UrlsRedirectMetaData
{
    public const FIELD_FROM = 'from';
    public const FIELD_TO = 'to';
    public const FIELD_LAST_UPDATE = 'last_update';
}
