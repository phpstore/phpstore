<?php

namespace App\MetaData;

interface MenuMetaData
{
    public const FIELD_LEVEL = 'level';
    public const FIELD_PARENT_ID = 'parentId';
    public const FIELD_ORDER = 'order';

    public const VALUE_FIRST_LEVEL = 1;
    public const VALUE_SECOND_LEVEL = 2;
}
