<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 07.01.2019
 * Time: 14:11
 */

namespace App\Helper\DoctrineHelper;

use App\Helper\Walkers\SqlWalkerCustom;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Parameter;

class DoctrineHelper
{
    const MODE_STRAIGHT_JOIN = 'STRAIGHT_JOIN';
    const MODE_NORMAL = '';
    const CODE_EMPTY_ARRAY_PARAMETER = 1;
    const EMPTY_SELECT = 'SELECT * FROM (SELECT NULL) a WHERE 0!=0;';

    protected static $intParamsTypes = [
        Type::INTEGER               => \PDO::PARAM_INT,
        \PDO::PARAM_INT             => \PDO::PARAM_INT,
        Connection::PARAM_INT_ARRAY => \PDO::PARAM_INT
    ];

    /**
     * @param Query $query
     * @param string $selectMode
     * @param bool $replaceAliases
     * @return \Doctrine\DBAL\Driver\ResultStatement
     */
    public static function getStatement(Query $query, $selectMode = self::MODE_NORMAL, $replaceAliases = true)
    {
        $sql = self::getSql($query, $selectMode, $replaceAliases);
        return $query->getEntityManager()->getConnection()->executeQuery($sql);
    }

    /**
     * @param $mode
     * @return string
     */
    private static function validateMode($mode)
    {
        if ($mode === self::MODE_NORMAL || $mode === self::MODE_STRAIGHT_JOIN) {
            return $mode;
        }
        return '';
    }

    /**
     * @param Query $query
     * @return \Doctrine\ORM\Query\ResultSetMapping|null
     */
    private static function getMapper(Query $query)
    {
        $queryReflection = new \ReflectionClass($query);

        $queryProperty = $queryReflection->getProperty('_parserResult');
        $queryProperty->setAccessible(true);
        if ($value = $queryProperty->getValue($query)) {
            return $value->getResultSetMapping();
        }
        return null;
    }

    /**
     * @param Parameter $parameter
     * @param Query $query
     * @return callable
     */
    protected static function getQuoteCallable(Parameter $parameter, Query $query)
    {
        $types         = $parameter->getType();
        $quoteFunction = array($query->getEntityManager()->getConnection(), 'quote');
        // for multiIn cast types
        if (\is_array($types)) {
            $quoteFunctions = [];
            foreach ($types as $typeKey => $type) {
                if (isset(self::$intParamsTypes[$type])) {
                    $quoteFunctions[$typeKey] = 'intval';
                } else {
                    $quoteFunctions[$typeKey] = &$quoteFunction;
                }
            }
            return function (array $params) use (&$quoteFunctions, &$quoteFunction) {
                foreach ($params as $index => &$param) {
                    if (isset($quoteFunctions[$index])) {
                        $param = $quoteFunctions[$index]($param);
                    } else {
                        $param = $quoteFunction($param);
                    }
                }
                unset($quoteFunctions, $param, $quoteFunction);
                return '(' . \implode(',', $params) . ')';
            };
        }
        if (isset(self::$intParamsTypes[$types])) {
            return 'intval';
        }

        return $quoteFunction;
    }

    /**
     * @param Query $query
     * @param string $selectMode
     * @param bool $replaceAliases
     * @return string
     * @throws Query\QueryException
     */
    public static function getSql(Query $query, $selectMode = self::MODE_NORMAL, $replaceAliases = true)
    {
        $query->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, SqlWalkerCustom::getClassName());
        try {
            $sql = $query->getSQL();
            $sql = explode(' ', $sql, 2);
            if (count($sql) !== 2) {
                throw new Query\QueryException('Empty array', self::CODE_EMPTY_ARRAY_PARAMETER);
            }
            $sql        = array_map('trim', $sql);
            $selectMode = self::validateMode($selectMode);
            $sql        = "{$sql[0]} $selectMode {$sql[1]}";
            $rsm        = $replaceAliases ? self::getMapper($query) : false;
            if ($rsm) {
                $fieldMapping = array_replace($rsm->fieldMappings, $rsm->scalarMappings);
                foreach (array_flip($fieldMapping) as $goodAlias => $badAlias) {
                    $sql = preg_replace(
                        '/AS ' . preg_quote($badAlias, '/') . '([,])?/',
                        'AS "' . $goodAlias . '"$1',
                        $sql,
                        1
                    );
                }
            }

            $search  = array();
            $replace = array();
            foreach ($query->getParameters() as $parameter) {
                $value         = $parameter->getValue();
                if (is_array($value)) {
                    if (!count($value)) {
                        throw new Query\QueryException('Empty array', self::CODE_EMPTY_ARRAY_PARAMETER);
                    }
                    $quoteFunction = self::getQuoteCallable($parameter, $query);
                    $replace[] = implode(', ', array_map($quoteFunction, $value));
                } else {
                    // DON'T USE self::getQuoteCallable, because of ann2_url.show is enum
                    $replace[] = $query->getEntityManager()->getConnection()->quote($value);
                }
                // Remember about query cache before some changes
                $search[] = '::??' . md5('::??' . $parameter->getName());
            }
            $sql = str_replace($search, $replace, $sql);
        } catch (Query\QueryException $e) {
            if ($e->getCode() == self::CODE_EMPTY_ARRAY_PARAMETER) {
                $sql = self::EMPTY_SELECT;
            } else {
                throw $e;
            }
        }
        return $sql;
    }
}