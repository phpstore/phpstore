<?php

namespace App\Repository;

use App\Entity\UrlType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UrlType|null find($id, $lockMode = null, $lockVersion = null)
 * @method UrlType|null findOneBy(array $criteria, array $orderBy = null)
 * @method UrlType[]    findAll()
 * @method UrlType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrlTypeRepository extends ServiceEntityRepository
{
    const FIELD_ID = 'id';

    const PROPERTY_URL_TYPE = 'urlType';
    const PROPERTY_DISPLAY_NAME = 'displayName';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UrlType::class);
    }

    public function getAllUrlTypes()
    {
        return $this->createQueryBuilder('ut')
            ->select()
            ->getQuery()
            ->getResult(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $urlTypeData
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setUrlType($urlTypeData)
    {
        $urlTypeId = isset($urlTypeData[self::FIELD_ID]) ? $urlTypeData[self::FIELD_ID] : 0;
        $urlType = $this->find($urlTypeId);
        if (!($urlType instanceof UrlType)) {
            $urlType = new UrlType();
        }

        $urlType->setUrlType($urlTypeData[self::PROPERTY_URL_TYPE]);
        $urlType->setDisplayName($urlTypeData[self::PROPERTY_DISPLAY_NAME]);
        $this->_em->persist($urlType);
        $this->_em->flush();
    }

    /**
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteById($id)
    {
        $urlType = $this->find($id);
        $this->_em->remove($urlType);

        $this->_em->flush();
    }
}
