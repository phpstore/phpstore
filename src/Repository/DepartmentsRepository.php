<?php

namespace App\Repository;

use App\Entity\Departments;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Departments|null find($id, $lockMode = null, $lockVersion = null)
 * @method Departments|null findOneBy(array $criteria, array $orderBy = null)
 * @method Departments[]    findAll()
 * @method Departments[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DepartmentsRepository extends ServiceEntityRepository
{
    const PROPERTY_ID = 'id';
    const PROPERTY_NAME = 'name';
    const PROPERTY_H1_SPAN = 'h1Span';

    const FIELD_ID = 'id';
    const FIELD_URL = 'url';
    const FIELD_TITLE = 'title';
    const FIELD_H1 = 'h1';
    const FIELD_KEYWORDS = 'keywords';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_PRODUCT_TYPE_ID = 'product_type_id';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Departments::class);
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllDepartments()
    {
        $query = 'SELECT d.*
                  FROM departments d';

        $stmt = $this->_em->getConnection()->prepare($query);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @param array $departmentData
     * @return int|null
     * @throws \Doctrine\ORM\ORMException
     */
    public function setDepartment($departmentData)
    {
        $department = new Departments();
        $department->setName($departmentData[self::PROPERTY_NAME]);
        $department->setUrl($departmentData[self::FIELD_URL]);
        $department->setTitle($departmentData[self::FIELD_TITLE]);
        $department->setH1($departmentData[self::FIELD_H1]);
        $department->setH1Span($departmentData[self::PROPERTY_H1_SPAN]);
        $department->setKeywords($departmentData[self::FIELD_KEYWORDS]);
        $department->setDescription($departmentData[self::FIELD_DESCRIPTION]);
        $this->_em->persist($department);
        $this->_em->flush($department);

        return $department->getId();
    }

    /**
     * @param $departmentData
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateDepartment($departmentData)
    {
        $departmentId = $departmentData[self::PROPERTY_ID];
        $department = $this->find($departmentId);
        $department->setName($departmentData[self::PROPERTY_NAME]);
        $department->setTitle($departmentData[self::FIELD_TITLE]);
        $department->setH1($departmentData[self::FIELD_H1]);
        $department->setH1Span($departmentData[self::PROPERTY_H1_SPAN]);
        $department->setKeywords($departmentData[self::FIELD_KEYWORDS]);
        $department->setDescription($departmentData[self::FIELD_DESCRIPTION]);
        $this->_em->persist($department);
        $this->_em->flush($department);
    }

    /**
     * @param $departmentData
     * @param $departmentId
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateDepartmentByDepartmentId($departmentData, $departmentId)
    {
        $em = $this->getEntityManager();

        $department = $this->find($departmentId);
        $department->setName($departmentData[self::PROPERTY_NAME]);

        $em->persist($department);
        $em->flush();
    }

    /**
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteById($id)
    {
        $department = $this->find($id);
        $this->_em->remove($department);

        $this->_em->flush();
    }
}
