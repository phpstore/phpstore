<?php

namespace App\Repository;

use App\Entity\UsersSubscribe;
use App\Helper\DoctrineHelper\DoctrineHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UsersSubscribe|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersSubscribe|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersSubscribe[]    findAll()
 * @method UsersSubscribe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersSubscribeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UsersSubscribe::class);
    }

    /**
     * @param $name
     * @param $email
     * @param $departmentId
     * @param $categoryId
     * @return int
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setUserSubscribe($name, $email, $departmentId, $categoryId): int
    {
        $subscribe = new UsersSubscribe();
        $subscribe
            ->setName($name)
            ->setEmail($email)
            ->setDepartmentId($departmentId)
            ->setCategoryId($categoryId);
        $this->_em->persist($subscribe);

        $this->_em->flush();

        return $subscribe->getId();
    }

    /**
     * @param $name
     * @param $email
     * @param $departmentId
     * @param $categoryId
     * @return false|mixed
     */
    public function getUserSubscribeId($name, $email, $departmentId, $categoryId)
    {
        $qb = $this->createQueryBuilder('us')
            ->select('us.id')
            ->where('us.name = :name')
            ->andWhere('us.email = :email')
            ->andWhere('us.departmentId = :departmentId')
            ->andWhere('us.categoryId = :categoryId')
            ->setParameters([
                'name' => $name,
                'email' => $email,
                'departmentId' => $departmentId,
                'categoryId' => $categoryId
            ]);

        return DoctrineHelper::getStatement($qb->getQuery())->fetchColumn();
    }
}
