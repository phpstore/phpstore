<?php

namespace App\Repository;

use App\Entity\CategoriesMeta;
use App\Helper\DoctrineHelper\DoctrineHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CategoriesMeta|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoriesMeta|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoriesMeta[]    findAll()
 * @method CategoriesMeta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriesMetaRepository extends ServiceEntityRepository
{
    const FIELD_TITLE = 'title';
    const FIELD_H1 = 'h1';
    const FIELD_KEYWORDS = 'keywords';
    const FIELD_DESCRIPTION = 'description';

    const PROPERTY_H1_SPAN = 'h1Span';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CategoriesMeta::class);
    }

    /**
     * @param int $relationId
     * @param array $metaData
     * @throws DBALException
     */
    public function setMetaByRelationId($relationId, $metaData)
    {
        $query = 'INSERT INTO categories_meta (relation_id, title, h1, h1_span, keywords, description)
                VALUES
                    (:relationId, :title, :h1, :h1Span, :keywords, :description)
                ON DUPLICATE KEY UPDATE
                    title = VALUES(title),
                    h1 = VALUES(h1),
                    h1_span = VALUES(h1_span),
                    keywords = VALUES(keywords),
                    description = VALUES(description)';

        $params = [
            'relationId' => $relationId,
            'title' => $metaData[self::FIELD_TITLE],
            'h1' => $metaData[self::FIELD_H1],
            'h1Span' => $metaData[self::PROPERTY_H1_SPAN],
            'keywords' => $metaData[self::FIELD_KEYWORDS],
            'description' => $metaData[self::FIELD_DESCRIPTION]
        ];

        $this->_em->getConnection()->executeQuery($query, $params);
    }

    /**
     * @param $relationId
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getMetaByRelationId($relationId)
    {
        return $this->createQueryBuilder('cm')
            ->select()
            ->where('cm.relationId = :relationId')
            ->setParameter('relationId', $relationId)
            ->getQuery()
            ->getOneOrNullResult(\PDO::FETCH_ASSOC);

//        return DoctrineHelper::getStatement($qb->getQuery())->fetchAll(\PDO::FETCH_LAZY);
    }
}
