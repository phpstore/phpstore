<?php

namespace App\Repository;

use App\Entity\Articles;
use App\Entity\ArticlesViews;
use App\Helper\DoctrineHelper\DoctrineHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\ResultStatement;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Join;
use PDO;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Articles|null find($id, $lockMode = null, $lockVersion = null)
 * @method Articles|null findOneBy(array $criteria, array $orderBy = null)
 * @method Articles[]    findAll()
 * @method Articles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticlesRepository extends ServiceEntityRepository
{
    const FIELD_ID = 'id';
    const FIELD_TITLE = 'title';
    const FIELD_H1 = 'h1';
    const FIELD_KEYWORDS = 'keywords';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_TEXT = 'text';
    const FIELD_DATE = 'date';
    const FIELD_STATUS = 'status';

    const PROPERTY_CATEGORY_ID = 'categoryId';
    const PROPERTY_MAIN_IMAGE_PATH = 'mainImagePath';
    const PROPERTY_MAIN_IMAGE_ALT = 'mainImageAlt';

    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_PUBLISHED = 'published';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Articles::class);
    }

    /**
     * @param array $articleData
     * @return int
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setArticle($articleData)
    {
        $articleId = isset($articleData[self::FIELD_ID]) ? $articleData[self::FIELD_ID] : 0;
        $article = $this->find($articleId);
        if (!($article instanceof Articles)) {
            $article = new Articles();
        }

        $article->setTitle($articleData[self::FIELD_TITLE]);
        $article->setH1($articleData[self::FIELD_H1]);
        $article->setKeywords($articleData[self::FIELD_KEYWORDS]);
        $article->setDescription($articleData[self::FIELD_DESCRIPTION]);
        $article->setCategoryId($articleData[self::PROPERTY_CATEGORY_ID]);
        $article->setMainImagePath($articleData[self::PROPERTY_MAIN_IMAGE_PATH]);
        $article->setMainImageAlt($articleData[self::PROPERTY_MAIN_IMAGE_ALT]);
        $article->setText($articleData[self::FIELD_TEXT]);
        $article->setDate(date('Y-m-d'));
        $article->setStatus($articleData[self::FIELD_STATUS]);

        $this->_em->persist($article);
        $this->_em->flush();

        return $article->getId();
    }

    /**
     * @param $words
     * @return mixed[]
     * @throws DBALException
     */
    public function search($words)
    {
        $query = "SELECT id FROM articles WHERE ";
        foreach ($words as $word) {
            $query .= "`title` LIKE '%{$word}%' OR `text` LIKE '%{$word}%' OR";
        }
        $query = substr($query, 0, -2);

        return $this->_em->getConnection()->executeQuery($query)->fetchAll();
    }

    public function getProductsByCategoryId(array $categoryIds)
    {
        $qb = $this->createQueryBuilder('a')
            ->select()
            ->where('a.categoryId IN (:categoryId)')
            ->setParameter('categoryId', $categoryIds)
            ->orderBy('a.date', 'DESC');

        return DoctrineHelper::getStatement($qb->getQuery())->fetchAll();
    }

    public function getProducts($offset = 0, $limit = 0)
    {
        $qb = $this->createQueryBuilder('a')
            ->select();
        if ($offset > 0) {
            $qb->setFirstResult($offset);
        }
        if ($limit > 0) {
            $qb->setMaxResults($limit);
        }

        return DoctrineHelper::getStatement($qb->getQuery())->fetchAll();
    }

    /**
     * @param $productId
     * @return ResultStatement
     * @throws NonUniqueResultException
     */
    public function getArticleById($productId): array
    {
        return $this->createQueryBuilder('a')
            ->select()
            ->where('a.id = :productId')
            ->setParameter('productId', $productId)
            ->getQuery()
            ->getOneOrNullResult(PDO::FETCH_ASSOC);
    }

    public function getProductsByIds($productIds)
    {
        return $this->createQueryBuilder('a')
            ->select()
            ->where('a.id IN (:productIds)')
            ->setParameter('productIds', $productIds)
            ->getQuery()
            ->getResult(PDO::FETCH_ASSOC);
    }

    /**
     * @param $articleId
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteById($articleId)
    {
        $product = $this->find($articleId);
        $this->_em->remove($product);

        $this->_em->flush();
    }

    public function getTopViewsArticleIds($limit = 0)
    {
        $qb = $this->createQueryBuilder('a')
            ->select('a.id')
            ->leftJoin(ArticlesViews::class, 'av', Join::WITH, 'a.id = av.articleId')
            ->orderBy('av.views', 'DESC');

        if ($limit > 0) {
            $qb->setMaxResults($limit);
        }

        return DoctrineHelper::getStatement($qb->getQuery())->fetchAll(PDO::FETCH_COLUMN);
    }

    public function getCountArticlesByCategoryId(int $categoryId): int
    {
        $qb = $this->createQueryBuilder('a')
            ->select('COUNT(a.id) as count')
            ->where('a.categoryId = :categoryId')
            ->setParameter('categoryId', $categoryId);

        return (int) DoctrineHelper::getStatement($qb->getQuery())->fetchColumn();
    }
}
