<?php

namespace App\Repository;

use App\Entity\ArticlesRating;
use App\Helper\DoctrineHelper\DoctrineHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ArticlesRating|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticlesRating|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticlesRating[]    findAll()
 * @method ArticlesRating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticlesRatingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ArticlesRating::class);
    }

    public function getRatingByArticleId($articleId)
    {
        $qb = $this->createQueryBuilder('ar')
            ->select('ar.rating')
            ->where('ar.articleId = :articleId')
            ->setParameter('articleId', $articleId);

        return DoctrineHelper::getStatement($qb->getQuery())->fetchColumn();
    }

    /**
     * @param $articleId
     * @param $rating
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setArticleRating($articleId, $rating): void
    {
        $articleRating = $this->findOneBy(compact('articleId'));
        if (!($articleRating instanceof ArticlesRating)) {
            $articleRating = new ArticlesRating();
        }
        $articleRating->setArticleId($articleId);
        $articleRating->setRating($rating);

        $this->_em->persist($articleRating);
        $this->_em->flush();
    }
}
