<?php

namespace App\Repository;

use App\Entity\BlockElementCategories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BlockElementCategories|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlockElementCategories|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlockElementCategories[]    findAll()
 * @method BlockElementCategories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlockElementCategoriesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BlockElementCategories::class);
    }

    public function getElementsContent($departmentId, $categoryIds): array
    {
        $elements = $this->findBy([
            BlockElementCategories::FIELD_DEPARTMENT_ID => $departmentId,
            BlockElementCategories::FIELD_CATEGORY_ID => $categoryIds
        ]);
        $result = [];
        foreach ($elements as $element) {
            $result[$element->getCategoryId()] = $element;
        }

        return $result;
    }

    /**
     * @param $elements
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setElementsContent(array $elements): void
    {
        foreach ($elements as $elementId => $element) {
            $elementId = $element[BlockElementCategories::FIELD_ID] ?? 0;
            $blockElement = $this->find($elementId);
            if (!($blockElement instanceof BlockElementCategories)) {
                $blockElement = new BlockElementCategories();
                $blockElement
                    ->setDepartmentId($element['departmentId'])
                    ->setCategoryId($element['categoryId']);
            }
            $blockElement
                ->setTitle($element[BlockElementCategories::FIELD_TITLE] ?? '')
                ->setDescription($element[BlockElementCategories::FIELD_DESCRIPTION] ?? '')
                ->setImg($element[BlockElementCategories::FIELD_IMG] ?? '')
                ->setAlt($element[BlockElementCategories::FIELD_ALT] ?? '')
                ->setOrder($element[BlockElementCategories::FIELD_ORDER] ?? 0);
            $this->_em->persist($blockElement);
        }

        $this->_em->flush();
    }
}
