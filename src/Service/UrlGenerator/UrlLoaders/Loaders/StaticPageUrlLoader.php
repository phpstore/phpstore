<?php

namespace App\Service\UrlGenerator\UrlLoaders\Loaders;

use App\Repository\UrlsRepository;
use App\Service\ProductLoader\ProductLoaderFactory;
use App\Service\StaticPage\StaticPageService;
use App\Service\Url\UrlService;
use App\Service\UrlGenerator\CatalogUrlsFormatter\CatalogUrlsArrayFormatter;
use App\Service\UrlGenerator\Logger\Logger;
use App\Service\UrlType\UrlTypeService;

class StaticPageUrlLoader extends AbstractUrlLoader
{
    const URL_TYPE = 'static';

    /** @var StaticPageService */
    private $staticPageService;

    public function __construct(
        Logger $logger,
        UrlTypeService $urlTypeService,
        UrlService $urlService,
        ProductLoaderFactory $productLoaderFactory,
        StaticPageService $staticPageService
    ) {
        $this->staticPageService = $staticPageService;
        parent::__construct($logger, $urlTypeService, $urlService, $productLoaderFactory);
    }

    public function getLoaderType()
    {
        return self::URL_TYPE;
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function process()
    {
        $this->logger->writeln($this->getStartLogString());

        $staticPages = $this->staticPageService->getAllStaticPages();

        $hash = $this->generateHash();

        $resultUrls = [];
        $countOpened = 0;
        $countClosed = 0;
        foreach ($staticPages as $staticPageId => $staticPage) {
            $resultUrls[$staticPageId] = $staticPage;
            $resultUrls[$staticPageId][UrlsRepository::PROPERTY_DEPARTMENT_ID] = 0;
            $resultUrls[$staticPageId][UrlsRepository::PROPERTY_ENTITY_ID] = $staticPageId;
            $resultUrls[$staticPageId][UrlsRepository::PROPERTY_URL_TYPE_ID] = $this->urlTypeId;
            $resultUrls[$staticPageId][UrlsRepository::FIELD_OPEN]
                = UrlsRepository::VALUE_OPEN_YES;
            $resultUrls[$staticPageId][UrlsRepository::FIELD_HASH] = $hash;
        }

        $resultUrls = $this->buildUrls($resultUrls);
        $resultUrls = CatalogUrlsArrayFormatter::format($resultUrls);
        $resultUrls = $this->filterUrls($resultUrls);

        if (empty($resultUrls)) {
            return;
        }
        $countUrls = count($resultUrls);
        $countOpened = $countUrls;
        $this->urlService->setCatalogUrls($resultUrls);

        $countDeleted = $this->urlService->getCountUnusedCatalogUrls($this->urlTypeId, $hash);
        $this->urlService->deleteUnusedCatalogUrls($this->urlTypeId, $hash);

        $this->logger->writeln([
            $this->getFinishedLogString($countUrls, $countOpened, $countClosed, $countDeleted),
            ''
        ]);
    }

    protected function buildUrls(array $entities)
    {
        return $entities;
    }
}
