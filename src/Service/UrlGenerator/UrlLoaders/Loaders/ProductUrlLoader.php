<?php

namespace App\Service\UrlGenerator\UrlLoaders\Loaders;

use App\Entity\Articles;
use App\Repository\ArticlesRepository;
use App\Repository\UrlsRepository;
use App\Repository\DepartmentsRepository;
use App\Service\DepartmentCategory\DepartmentCategoriesService;
use App\Service\Department\DepartmentService;
use App\Service\ProductLoader\ProductLoaderFactory;
use App\Service\Url\UrlService;
use App\Service\UrlGenerator\CatalogUrlsFormatter\CatalogUrlsArrayFormatter;
use App\Service\UrlGenerator\Logger\Logger;
use App\Service\UrlGenerator\UrlStringBuilder\UrlStringBuilder;
use App\Service\UrlType\UrlTypeService;

class ProductUrlLoader extends AbstractUrlLoader
{
    const URL_TYPE = 'prod';
    const URL_PREFIX = 'prod';

    /** @var ArticlesRepository */
    private $articlesRepository;

    /** @var UrlStringBuilder */
    private $urlStringBuilder;

    /** @var DepartmentCategoriesService */
    private $departmentCategoriesService;

    /** @var DepartmentService */
    private $departmentService;

    public function __construct(
        Logger $logger,
        UrlTypeService $urlTypeService,
        UrlService $urlService,
        ProductLoaderFactory $productLoaderFactory,
        ArticlesRepository $articlesRepository,
        UrlStringBuilder $urlStringBuilder,
        DepartmentCategoriesService $departmentCategoriesService,
        DepartmentService $departmentService
    ) {
        parent::__construct($logger, $urlTypeService, $urlService, $productLoaderFactory);

        $this->articlesRepository = $articlesRepository;
        $this->urlStringBuilder = $urlStringBuilder;
        $this->departmentCategoriesService = $departmentCategoriesService;
        $this->departmentService = $departmentService;
    }

    public function getLoaderType()
    {
        return self::URL_TYPE;
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function process()
    {
        $this->logger->writeln($this->getStartLogString());

        $articles = $this->articlesRepository->getProducts();

        $hash = $this->generateHash();

        $resultUrls = [];
        $countOpened = 0;
        $countClosed = 0;
        foreach ($articles as $article) {
            $articleId = $article[ArticlesRepository::FIELD_ID];
            $url = $article;
            $url[UrlsRepository::PROPERTY_URL_TYPE_ID] = $this->urlTypeId;
            $url[UrlsRepository::PROPERTY_ENTITY_ID] = $articleId;
            if ($article[ArticlesRepository::FIELD_STATUS] === ArticlesRepository::STATUS_PUBLISHED) {
                $open = UrlsRepository::VALUE_OPEN_YES;
                ++$countOpened;
            } else {
                $open = UrlsRepository::VALUE_OPEN_NO;
                ++$countClosed;
            }
            $url[UrlsRepository::FIELD_OPEN] = $open;
            $url[UrlsRepository::FIELD_HASH] = $hash;

            $articleCategoryId = $article[ArticlesRepository::PROPERTY_CATEGORY_ID];
            $categoryRelations = $this->departmentCategoriesService->getRelationsByCategoryId($articleCategoryId);
            foreach ($categoryRelations[DepartmentCategoriesService::KEY_DEPARTMENTS] as $department) {
                $url[UrlsRepository::PROPERTY_DEPARTMENT_ID] = $department[DepartmentsRepository::FIELD_ID];
                $resultUrls[] = $url;
            }
        }

        $resultUrls = $this->buildUrls($resultUrls);
        $resultUrls = CatalogUrlsArrayFormatter::format($resultUrls);
        $resultUrls = $this->filterUrls($resultUrls);

        if (empty($resultUrls)) {
            return;
        }
        $countUrls = count($resultUrls);
        $this->urlService->setCatalogUrls($resultUrls);

        $countDeleted = $this->urlService->getCountUnusedCatalogUrls($this->urlTypeId, $hash);
        $this->urlService->deleteUnusedCatalogUrls($this->urlTypeId, $hash);

        $this->logger->writeln([
            $this->getFinishedLogString($countUrls, $countOpened, $countClosed, $countDeleted),
            ''
        ]);

        return;
    }

    public function buildUrls(array $entities)
    {
        foreach ($entities as $key => $entity) {
            $entities[$key][UrlsRepository::FIELD_URL] = $this->formatUrlFromTitle($entity[Articles::FIELD_H1]);
        }

        return $entities;
    }

    public function formatUrlFromTitle($title): string
    {
        return self::URL_PREFIX . '/' . 'articles' . '/' . $this->urlStringBuilder->buildUrl($title);
    }
}
