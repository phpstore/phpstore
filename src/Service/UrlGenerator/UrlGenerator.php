<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 14.03.2019
 * Time: 22:42
 */

namespace App\Service\UrlGenerator;

use App\Repository\UrlTypeRepository;
use App\Service\UrlGenerator\UrlLoaders\Loaders\UrlLoaderInterface;
use App\Service\UrlGenerator\UrlLoaders\UrlLoaderFactory;
use App\Service\UrlType\UrlTypeService;

class UrlGenerator
{
    /** @var UrlLoaderFactory */
    private $urlLoaderFactory;

    /** @var UrlTypeService */
    private $urlTypeService;

    public function __construct(
        UrlLoaderFactory $urlLoaderFactory,
        UrlTypeService $urlTypeService
    ) {
        $this->urlLoaderFactory = $urlLoaderFactory;
        $this->urlTypeService = $urlTypeService;
    }

    public function run($urlTypes = [])
    {
        if (empty($urlTypes)) {
            $urlTypes = $this->getAllUrlTypes();
        }

        $urlLoaders = $this->urlLoaderFactory->buildUrlLoaders($urlTypes);
        /** @var UrlLoaderInterface $urlLoader */
        foreach ($urlLoaders as $urlLoader) {
            $urlLoader->process();
        }
    }

    /**
     * @return array
     */
    private function getAllUrlTypes()
    {
        $urlTypeEntries = $this->urlTypeService->getAllUrlTypes();
        $urlTypes = array_column($urlTypeEntries, UrlTypeRepository::PROPERTY_URL_TYPE);

        return $urlTypes;
    }
}
