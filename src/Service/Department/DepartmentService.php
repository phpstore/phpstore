<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 01.01.2019
 * Time: 23:01
 */

namespace App\Service\Department;

use App\Components\ProductLoader\ProductLoaderService;
use App\Repository\DepartmentsRepository;
use App\Service\Url\UrlService;

class DepartmentService
{
    /** @var DepartmentsRepository */
    private $departmentsRepository;

    /** @var UrlService */
    private $urlService;

    private $departmentEntries = [];

    /**
     * DepartmentService constructor.
     * @param DepartmentsRepository $departmentsRepository
     * @param UrlService $urlService
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __construct(
        DepartmentsRepository $departmentsRepository,
        UrlService $urlService
    ) {
        $this->departmentsRepository = $departmentsRepository;
        $this->urlService = $urlService;

        $this->init();
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    private function init()
    {
        if (empty($this->departmentEntries)) {
            $departments = $this->departmentsRepository->getAllDepartments();
            foreach ($departments as $key => $department) {
                $this->departmentEntries[$department[DepartmentsRepository::PROPERTY_ID]] = $department;
            }
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function getDepartmentEntryById($id)
    {
        if (isset($this->departmentEntries[$id])) {
            return $this->departmentEntries[$id];
        }

        return [];
    }

    /**
     * @return array
     */
    public function getAllDepartments()
    {
        return $this->departmentEntries;
    }

    /**
     * @param string $name
     * @return int
     */
    public function getDepartmentIdByName($name)
    {
        $departmentNames = array_column(
            $this->departmentEntries,
            DepartmentsRepository::PROPERTY_NAME,
            DepartmentsRepository::PROPERTY_ID
        );
        foreach ($departmentNames as $departmentId => $departmentName) {
            $departmentNames[$departmentId] = mb_strtolower($departmentName);
        }
        $departmentNames = array_flip($departmentNames);
        if (isset($departmentNames[$name])) {
            return $departmentNames[$name];
        }

        return 0;
    }

    /**
     * @param string $url
     * @return int
     */
    public function getDepartmentIdByUrl($url)
    {
        $urls = array_column(
            $this->departmentEntries,
            DepartmentsRepository::FIELD_URL,
            DepartmentsRepository::PROPERTY_ID
        );
        return (int)array_search($url, $urls);
    }

    /**
     * @param array $departmentData
     * @throws \Doctrine\ORM\ORMException
     */
    public function setDepartment($departmentData)
    {
        $this->departmentsRepository->setDepartment($departmentData);
    }

    /**
     * @param $departmentData
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateDepartment($departmentData)
    {
        $this->departmentsRepository->updateDepartment($departmentData);
    }

    /**
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteDepartmentById($id)
    {
        $this->departmentsRepository->deleteById($id);
    }
}
