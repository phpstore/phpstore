<?php

namespace App\Service\SitemapGenerator;

use App\Entity\Urls;
use App\Entity\UrlType;
use App\Repository\SitemapGenerationRepository;
use App\Repository\UrlsRepository;
use App\Service\Logger\Logger;
use App\Service\Url\UrlService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use DOMDocument;

class SitemapGenerator implements SitemapGeneratorInterface
{
    public const CHANGEFREQ_WEEKLY = 'weekly';
    public const CHANGEFREQ_MONTHLY = 'monthly';
    /** @var UrlsRepository */
    private $urlsRepository;

    /** @var SitemapGenerationRepository */
    private $sitemapGenerationRepository;

    /** @var UrlService */
    private $urlService;

    /** @var Logger */
    private $logger;

    public function __construct(
        UrlsRepository $urlsRepository,
        SitemapGenerationRepository $sitemapGenerationRepository,
        UrlService $urlService,
        Logger $logger
    ) {
        $this->urlsRepository = $urlsRepository;
        $this->sitemapGenerationRepository = $sitemapGenerationRepository;
        $this->urlService = $urlService;
        $this->logger = $logger;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function run(): void
    {
        $urls = $this->urlsRepository->getOpenUrls();
        $urls = $this->sortUrls($urls);

        $totalUrls = count($urls);
        $productUrls = $this->getCountUrlsByUrlTypeId($urls, UrlType::URL_TYPE_ID_PRODUCT);
        $categoryUrls = $this->getCountUrlsByUrlTypeId($urls, UrlType::URL_TYPE_ID_CATEGORY);
        $departmentUrls = $this->getCountUrlsByUrlTypeId($urls, UrlType::URL_TYPE_ID_DEPARTMENT);
        $staticUrls = $this->getCountUrlsByUrlTypeId($urls, UrlType::URL_TYPE_ID_STATIC_PAGE);

        $sitemap = new DOMDocument('1.0', 'utf-8');
        $sitemap->preserveWhiteSpace = false;
        $sitemap->formatOutput = true;

        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

        /** @var Urls $url */
        foreach ($urls as $url) {
            $changefreq = $this->getChangeFreq($url->getUrlTypeId());
            $priority = $this->getPriority($url->getUrlTypeId());

            $sitemapUrl = $sitemap->createElement('url');

            $loc = $sitemap->createElement('loc', $this->urlService->getDomainUrl() . '/' . ltrim($url->getUrl(), '/'));
            $lastMod = $sitemap->createElement('lastmod', $url->getLastUpdate()->format('Y-m-d'));
            $changefreq = $sitemap->createElement('changefreq', $changefreq);
            $priority = $sitemap->createElement('priority', $priority);

            $sitemapUrl->appendChild($loc);
            $sitemapUrl->appendChild($lastMod);
            $sitemapUrl->appendChild($changefreq);
            $sitemapUrl->appendChild($priority);

            $urlset->appendChild($sitemapUrl);
        }
        $sitemap->appendChild($urlset);

        file_put_contents('sitemap.xml', $sitemap->saveXML());
        file_put_contents('public/sitemap.xml', $sitemap->saveXML());

        $this->sitemapGenerationRepository->setSitemapGenerationStatus(
            $totalUrls,
            $productUrls,
            $categoryUrls,
            $departmentUrls,
            $staticUrls
        );

        $this->logger->writeln('Finished: Total ' . $totalUrls
            . ' | ' . 'Product ' . $productUrls
            . ' | ' . 'Category ' . $categoryUrls
            . ' | ' . 'Department ' . $departmentUrls
            . ' | ' . 'Static ' . $staticUrls);
    }

    private function getChangeFreq($urlTypeId): string
    {
        $changeFreq = self::CHANGEFREQ_WEEKLY;
        switch ($urlTypeId) {
            case UrlType::URL_TYPE_ID_PRODUCT:
            case UrlType::URL_TYPE_ID_STATIC_PAGE:
                $changeFreq = self::CHANGEFREQ_MONTHLY;
        }

        return $changeFreq;
    }

    private function getPriority($urlTypeId)
    {
        $priority = 0.5;
        if ($urlTypeId === UrlType::URL_TYPE_ID_PRODUCT) {
            $priority = 1;
        }

        return $priority;
    }

    /**
     * @param Urls[] $urls
     * @return Urls[]
     */
    private function sortUrls($urls)
    {
        $homeUrl = [];
        $productUrls = [];
        $departmentUrls = [];
        $categoryUrls = [];
        $staticUrls = [];

        foreach ($urls as $index => $url) {
            switch ($url->getUrlTypeId()) {
                case UrlType::URL_TYPE_ID_DEPARTMENT:
                    $departmentUrls[] = $url;
                    break;
                case UrlType::URL_TYPE_ID_CATEGORY:
                    $categoryUrls[] = $url;
                    break;
                case UrlType::URL_TYPE_ID_PRODUCT:
                    $productUrls[] = $url;
                    break;
                case UrlType::URL_TYPE_ID_STATIC_PAGE:
                    if ($url->getUrl() === '/') {
                        $homeUrl[] = $url;
                    } else {
                        $staticUrls[] = $url;
                    }
                    break;
                default:
                    break;
            }
        }

        $result = array_merge($homeUrl, $productUrls, $categoryUrls, $departmentUrls, $staticUrls);

        return $result;
    }

    /**
     * @param Urls[] $urls
     * @param int $urlTypeId
     * @return int
     */
    private function getCountUrlsByUrlTypeId(array $urls, int $urlTypeId): int
    {
        $count = 0;
        foreach ($urls as $url) {
            if ($url->getUrlTypeId() === $urlTypeId) {
                $count++;
            }
        }

        return $count;
    }
}
