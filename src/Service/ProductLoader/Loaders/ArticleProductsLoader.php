<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 19.03.2019
 * Time: 23:14
 */

namespace App\Service\ProductLoader\Loaders;

use App\Entity\Articles;
use App\Entity\Categories;
use App\Entity\Departments;
use App\Repository\ArticlesImagesRepository;
use App\Repository\ArticlesRatingRepository;
use App\Repository\ArticlesRepository;
use App\Repository\ArticlesViewsRepository;
use App\Repository\UrlsRepository;
use App\Service\Category\CategoriesService;
use App\Service\Url\UrlService;

class ArticleProductsLoader implements ProductLoaderInterface
{
    const DEPEND_DEPARTMENT_ID = 1;
    const KEY_IMAGES = 'images';

    /** @var ArticlesRepository */
    private $articlesRepository;

    /** @var ArticlesImagesRepository */
    private $articlesImagesRepository;

    /** @var ArticlesViewsRepository */
    private $articlesViewsRepository;

    /** @var UrlsRepository */
    private $catalogUrlsRepository;

    /** @var ArticlesRatingRepository */
    private $articlesRatingRepository;

    /** @var CategoriesService */
    private $categoriesService;

    public function __construct(
        ArticlesRepository $articlesRepository,
        ArticlesImagesRepository $articlesImagesRepository,
        ArticlesViewsRepository $articlesViewsRepository,
        UrlsRepository $catalogUrlsRepository,
        ArticlesRatingRepository $articlesRatingRepository,
        CategoriesService $categoriesService
    ) {
        $this->articlesRepository = $articlesRepository;
        $this->articlesImagesRepository = $articlesImagesRepository;
        $this->articlesViewsRepository = $articlesViewsRepository;
        $this->catalogUrlsRepository = $catalogUrlsRepository;
        $this->articlesRatingRepository = $articlesRatingRepository;
        $this->categoriesService = $categoriesService;
    }

    public function getProductsByIds($productIds)
    {
        $products = $this->articlesRepository->getProductsByIds($productIds);
        $productImages = $this->articlesImagesRepository->getImagesByArticleIds($productIds);
        $productViews = $this->articlesViewsRepository->getViewsByProductIds($productIds);
        $productUrls = $this->catalogUrlsRepository->getProductOpenUrlsByProductIds(
            self::DEPEND_DEPARTMENT_ID,
            $productIds
        );
        foreach ($products as $index => $product) {
            $categoryEntry = $this->categoriesService->getCategoryEntryById($product[Articles::FIELD_CATEGORY_ID]);
            $products[$index]['category'] = $categoryEntry[Categories::FIELD_NAME];
            $productId = $product[ArticlesRepository::FIELD_ID];
            if (!isset($productUrls[$productId])) {
                unset($products[$index]);
                continue;
            }
            $products[$index]['url'] = UrlService::formatUri($productUrls[$productId]);
            $images = [];
            foreach ($productImages as $productImage) {
                if ($productId == $productImage[ArticlesImagesRepository::PROPERTY_ARTICLE_ID]) {
                    $images[] = $productImage;
                }
            }
            $products[$index]['images'] = $images;
            $views = 0;
            if (isset($productViews[$productId])) {
                $views = $productViews[$productId];
            }
            $products[$index]['views'] = $views;
        }

        return $products;
    }

    /**
     * @param int $productId
     * @return array|\Doctrine\DBAL\Driver\ResultStatement
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getProductById($productId)
    {
        $product = $this->articlesRepository->getArticleById($productId);
        $productImages = $this->articlesImagesRepository->getImagesByArticleId($productId);
        $productViews = $this->articlesViewsRepository->getViewsByProductIds($productId);
        $productRating = (int) $this->articlesRatingRepository->getRatingByArticleId($productId);
        $productUrls = $this->catalogUrlsRepository->getProductOpenUrlsByProductIds(
            self::DEPEND_DEPARTMENT_ID,
            $productId
        );
        $categoryEntry = $this->categoriesService->getCategoryEntryById($product[Articles::FIELD_CATEGORY_ID]);
        $product['category'] = $categoryEntry[Categories::FIELD_NAME];
        $product['url'] = $productUrls[$productId] ?? '';
        $images = [];
        foreach ($productImages as $productImage) {
            if ($productId == $productImage[ArticlesImagesRepository::PROPERTY_ARTICLE_ID]) {
                $images[] = $productImage;
            }
        }
        $product['images'] = $images;
        $views = 0;
        if (isset($productViews[$productId])) {
            $views = $productViews[$productId];
        }
        if ($productRating > 0) {
            $productRating = '+' . $productRating;
        }
        $product['views'] = $views;
        $product['rating'] = $productRating;
        $text = $product['text'];
        $product['text'] = $text;

        return $product;
    }

    public function getProducts($offset = 0, $limit = 0)
    {
        $products = $this->articlesRepository->getProducts($offset, $limit);
        foreach ($products as $index => $product) {
            $products[$index]['departmentId'] = Departments::DEPARTMENT_ID_ARTICLE;
        }

        return $products;
    }

    public function getProductsByCategoryId($categoryId)
    {
        $products = $this->articlesRepository->getProductsByCategoryId([$categoryId]);
        foreach ($products as $index => $product) {
            $products[$index]['departmentId'] = Departments::DEPARTMENT_ID_ARTICLE;
        }

        return $products;
    }

    public function getProductsByCategoryIds($categoryIds, $sort)
    {
        $products = $this->articlesRepository->getProductsByCategoryId($categoryIds);
        $productIds = array_column($products, Articles::FIELD_ID);
        $productImages = $this->articlesImagesRepository->getImagesByArticleIds($productIds);
        $productViews = $this->articlesViewsRepository->getViewsByProductIds($productIds);
        $productUrls = $this->catalogUrlsRepository->getProductOpenUrlsByProductIds(
            self::DEPEND_DEPARTMENT_ID,
            $productIds
        );
        foreach ($products as $index => $product) {
            $categoryEntry = $this->categoriesService->getCategoryEntryById($product[Articles::FIELD_CATEGORY_ID]);
            $products[$index]['category'] = $categoryEntry[Categories::FIELD_NAME];
            $productId = $product[ArticlesRepository::FIELD_ID];
            if (!isset($productUrls[$productId])) {
                unset($products[$index]);
                continue;
            }
            $products[$index]['url'] = UrlService::formatUri($productUrls[$productId]);
            $images = [];
            foreach ($productImages as $productImage) {
                if ($productId == $productImage[ArticlesImagesRepository::PROPERTY_ARTICLE_ID]) {
                    $images[] = $productImage;
                }
            }
            $products[$index]['images'] = $images;
            $views = 0;
            if (isset($productViews[$productId])) {
                $views = $productViews[$productId];
            }
            $products[$index]['views'] = (int) $views;
        }

        if ($sort === 'view') {
            usort($products, function ($a, $b) {
                if ($a['views'] === $b['views']) {
                    return 0;
                }
                return ($a['views'] > $b['views']) ? -1 : 1;
            });
        }

        return $products;
    }

    public function getAdminProductTemplate()
    {
        return 'admin/products/article/';
    }

    /**
     * @param array $product
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setProduct($product)
    {
        $product['text'] = htmlspecialchars($product['text']);
        $articleId = $this->articlesRepository->setArticle($product);
        $product[ArticlesRepository::FIELD_ID] = $articleId;
        if (!empty($product[self::KEY_IMAGES])) {
            $this->articlesImagesRepository->setArticleImages($product);
        }
    }

    public function isDependDepartment($departmentId)
    {
        return $departmentId === self::DEPEND_DEPARTMENT_ID;
    }

    /**
     * @param $productId
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteByProductId($productId)
    {
        $this->articlesRepository->deleteById($productId);
    }
}
