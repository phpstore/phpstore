<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 23.03.2019
 * Time: 11:33
 */

namespace App\Service\ProductLoader\Loaders;

class EmptyProductsLoader implements ProductLoaderInterface
{
    public function getProductsByIds($productIds)
    {
        return [];
    }

    public function getProductById($productId)
    {
        return [];
    }

    public function getProducts($offset = 0, $limit = 0)
    {
        return [];
    }

    public function getProductsByCategoryId($categoryId)
    {
        return [];
    }

    public function getProductsByCategoryIds($categoryIds, $sort)
    {
        return [];
    }

    public function getAdminProductTemplate()
    {
        return '';
    }

    public function setProduct($product)
    {
        return;
    }

    public function isDependDepartment($departmentId)
    {
        return false;
    }

    public function deleteByProductId($productId)
    {
        return;
    }
}
