<?php

namespace App\Service\Mailer;

use App\Service\Mailer\Configuration\Configuration;
use Swift_Mailer;

class MailerService
{
    /** @var Swift_Mailer */
    private $mailer;

    public function __construct(Configuration $configuration)
    {
        $this->init($configuration);
    }

    /**
     * @param Configuration $configuration
     */
    private function init(Configuration $configuration): void
    {
        $transport = new \Swift_SmtpTransport(
            $configuration->getHost(),
            $configuration->getPort(),
            $configuration->getEncryption()
        );
        $transport
            ->setAuthMode($configuration->getAuthMode())
            ->setUsername($configuration->getUser())
            ->setPassword($configuration->getPassword());

        $mailer = new \Swift_Mailer($transport);

        $this->mailer = $mailer;
    }

    public function send(\Swift_Message $message): void
    {
        $this->mailer->send($message);
    }
}
