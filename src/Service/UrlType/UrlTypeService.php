<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 17.03.2019
 * Time: 11:24
 */

namespace App\Service\UrlType;

use App\Repository\UrlTypeRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class UrlTypeService
{
    /** @var UrlTypeRepository */
    private $urlTypeRepository;

    private $urlTypeEntries = [];

    public function __construct(
        UrlTypeRepository $urlTypeRepository
    ) {
        $this->urlTypeRepository = $urlTypeRepository;

        $this->init();
    }

    private function init(): void
    {
        if (empty($this->urlTypeEntries)) {
            $urlTypes = $this->urlTypeRepository->getAllUrlTypes();
            foreach ($urlTypes as $urlType) {
                $this->urlTypeEntries[$urlType[UrlTypeRepository::FIELD_ID]] = $urlType;
            }
        }
    }

    public function getAllUrlTypes(): array
    {
        return $this->urlTypeEntries;
    }

    /**
     * @param array $urlTypeData
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setUrlType($urlTypeData): void
    {
        $this->urlTypeRepository->setUrlType($urlTypeData);
    }

    /**
     * @param int $id
     * @return array
     */
    public function getUrlTypeEntryById($id): array
    {
        return $this->urlTypeEntries[$id] ?? [];
    }

    /**
     * @param $id
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteUrlTypeById($id): void
    {
        $this->urlTypeRepository->deleteById($id);
    }

    /**
     * @param string $urlType
     * @return int
     */
    public function getUrlTypeIdByUrlType($urlType): int
    {
        $urlTypeToId = array_column(
            $this->urlTypeEntries,
            UrlTypeRepository::FIELD_ID,
            UrlTypeRepository::PROPERTY_URL_TYPE
        );
        return $urlTypeToId[$urlType] ?? 0;
    }
}
