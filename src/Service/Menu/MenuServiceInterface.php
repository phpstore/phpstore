<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 28.12.2018
 * Time: 18:11
 */

namespace App\Service\Menu;

use App\UrlData\UrlData;

interface MenuServiceInterface
{
    public function loadMenu(UrlData $urlData);
}