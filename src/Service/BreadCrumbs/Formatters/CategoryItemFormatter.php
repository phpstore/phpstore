<?php

namespace App\Service\BreadCrumbs\Formatters;

use App\Entity\Categories;
use App\Repository\UrlsRepository;
use App\Service\Category\CategoriesService;
use App\Service\Url\UrlService;
use App\UrlData\UrlData;

class CategoryItemFormatter implements BreadCrumbsItemFormatterInterface
{
    private const KEY = 'category';

    /** @var CategoriesService */
    private $categoriesService;

    /** @var UrlsRepository */
    private $urlsRepository;

    public function __construct(
        CategoriesService $categoriesService,
        UrlsRepository $urlsRepository
    ) {
        $this->categoriesService = $categoriesService;
        $this->urlsRepository = $urlsRepository;
    }

    public function getName(UrlData $urlData)
    {
        $categoryEntry = $this->categoriesService->getCategoryEntryById($urlData->getCategoryId());
        return $categoryEntry[Categories::FIELD_NAME];
    }

    public function getUrl(UrlData $urlData)
    {
        $url = $this->urlsRepository->getDepartmentCategoryUrl($urlData->getDepartmentId(), $urlData->getCategoryId());
        return UrlService::formatUri($url);
    }

    public static function getKey(): string
    {
        return self::KEY;
    }
}
