<?php

namespace App\Service\BreadCrumbs\Formatters;

use App\UrlData\UrlData;

class HomeItemFormatter implements BreadCrumbsItemFormatterInterface
{
    private const KEY = 'home';

    public function getName(UrlData $urlData)
    {
        return 'Главная';
    }

    public function getUrl(UrlData $urlData)
    {
        return '/';
    }

    public static function getKey()
    {
        return self::KEY;
    }
}
