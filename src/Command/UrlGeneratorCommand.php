<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 15.03.2019
 * Time: 23:50
 */

namespace App\Command;

use App\Service\UrlGenerator\UrlGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UrlGeneratorCommand extends Command
{
    protected static $defaultName = 'app:generate-urls';

    /** @var UrlGenerator */
    private $urlGenerator;

    public function __construct(
        UrlGenerator $urlGenerator,
        ?string $name = null
    ) {
        $this->urlGenerator = $urlGenerator;

        parent::__construct($name);
    }

    public function configure()
    {
        $this->setDescription('Generating urls')
            ->setHelp('This command generate the main site urls')
            ->addOption('url-type', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            'URL GENERATOR STARTED',
            ''
        ]);
        $urlTypes = $input->getOption('url-type');

        $this->urlGenerator->run($urlTypes);
    }
}
