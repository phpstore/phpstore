<?php

namespace App\Command;

use App\Service\SitemapGenerator\SitemapGeneratorInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SitemapGeneratorCommand extends Command
{
    protected static $defaultName = 'app:generate-sitemap';

    /** @var SitemapGeneratorInterface  */
    private $sitemapGenerator;

    public function __construct(SitemapGeneratorInterface $sitemapGenerator, string $name = null)
    {
        $this->sitemapGenerator = $sitemapGenerator;

        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->setDescription('Generating sitemap')
            ->setHelp('This command generate the main site urls');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            'SITEMAP GENERATING STARTED',
            ''
        ]);

        $this->sitemapGenerator->run();
    }
}
